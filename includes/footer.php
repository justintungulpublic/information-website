  <!-- Contact Section -->
  <div id="contact_us">
		<div class="container">
			<div class="row">
				<form id="contact-form" class="form-horizontal" role="form">
					<div class="contact-options col-md-5">
						<p>Got any questions or concerns? Interested to find out more about Snipe? Feel free to send us a message here and we’ll get in touch with you as soon as possible. We&#39;d love to hear from you!</p>
						<p>Kindly expect a reply from the Snipe team within 48 hours.</p>
					</div>
					
					<div class="form-section col-md-offset-1 col-md-6">
						<div class="form-group" id="name-field">
							<div class="col-lg-10">
								<input type="text" class="form-control contact-input" id="form-name" name="form-name" placeholder="Name" required>
							</div>
						</div>
						<div class="form-group" id="email-field">
							<div class="col-lg-10">
								<input type="email" class="form-control contact-input" id="form-email" name="form-email" placeholder="Email" required>
							</div>
						</div>
						<div class="form-group" id="user-type">
							<div class="col-lg-10">
								<select name="form-type" class="form-control" required>
  								<option value="" disabled selected>Select One</option>
									<option value="employee">Employee</option>
									<option value="merchant">Merchant</option>
									<option value="corporation">Corporation</option>
									<option value="other">Other</option>
								</select>
							</div>
						</div>
						<div class="form-group" id="message-field">
							<div class="col-lg-10">
								<textarea class="form-control contact-input" rows="6" id="form-message" name="form-message" placeholder="Message" required></textarea>
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-10">
								<div class="contact-failure alert alert-danger col-lg-10">
			  					Something went wrong. Please try again.
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-10">
								<button type="submit" class="btn btn-default col-xs-12 contact-input">Send</button>
							</div>
						</div>

						<div class="contact-success alert alert-success">
	  					Thank you! Your message has been sent successfully.
						</div>
					</div>
				</form>
			</div>
		</div>
  </div>

  <!-- Button trigger modal -->
	<!-- <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
	  Launch demo modal
	</button> -->

	<!-- Modal -->
<!-- 	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">Email Successful!</h4>
	      </div>
	      <div class="modal-body">
	        <div>Thank you for sending us an email...</div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" data-dismiss="modal" class="btn btn-primary" id="modal-ok-button"><span>OK</span></button>
	      </div>
	    </div>
	  </div>
	</div> -->


	<script src="plugins/jquery/2.0.3/jquery.min.js"></script>
	<script src="plugins/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="plugins/slick/1.6.0/slick.min.js"></script>
	<script src="plugins/viewportchecker/1.8.7/jquery.viewportchecker.min.js"></script>
  <script src="js/scripts.js"></script>

  <script type="text/javascript">

		jQuery(document).ready(function(){

		// run on DOM ready

		// grab target from URL hash (i.e. www.mysite.com/page-a.html#target-name)
		var target = window.location.hash;

		// only try to scroll to offset if target has been set in location hash
		if ( target != '' ){
		    var $target = jQuery(target); 
		    jQuery('html, body').stop().animate({
		    'scrollTop': $target.offset().top - 70}, 0, 
		    'swing',function () {
		    window.location.hash = target - 240 ;
		    });
		}

		});

  </script>

</body>
</html>
