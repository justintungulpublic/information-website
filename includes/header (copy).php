<!doctype html>
<html>
<head>
	<title>SnipePH</title>

	<link href='http://fonts.googleapis.com/css?family=Montserrat+Alternates:400,700' rel='stylesheet' type='text/css'>
	<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">


  <link rel="stylesheet" type="text/css" href="plugins/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="plugins/slick/1.6.0/slick.css"/>
	<link rel="stylesheet" type="text/css" href="plugins/slick/1.6.0/slick-theme.css"/>
	<link rel="stylesheet" type="text/css" href="plugins/animatecss/3.5.1/animate.css" />
	<link rel="stylesheet" type="text/css" href="styles/styles.css" />
</head>

<body>

  <!-- Fixed navbar -->
  <nav id="navbar_nav" class="navbar navbar-default navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" <?php echo ($_SESSION['page'] == 0 ) ? 'id="logo-button"' : 'href="index.php"'; ?> >
          <img class="navbar-main-logo" src="images/logo.png">
        </a>
      </div>
      <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
	        <li class="dropdown">
	          <a class="dropdown-toggle disabled" <?php echo ($_SESSION['page'] == 0 ) ? 'id="benefits-button"' : 'href="index.php"'; ; ?> 
               data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Benefits</a>
	          <ul class="dropdown-menu">
              <?php if ($_SESSION['page'] == 0) { ?>
              <li><a id="navbar-employees">Employees</a></li>
              <li><a id="navbar-merchants">Merchants</a></li>
              <li><a id="navbar-corporations">Corporations</a></li>
              <?php } else { ?>
              <li><a id="navbar-employees" href="index.php#employee">Employees</a></li>
              <li><a id="navbar-merchants" href="index.php#merchant">Merchants</a></li>
              <li><a id="navbar-corporations" href="index.php#corporate">Corporations</a></li>
              <?php }?>
	          </ul>
	        </li>
	        <li class="dropdown">
	          <a href="products.php" class="dropdown-toggle disabled" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Products</a>
	          <!-- <ul class="dropdown-menu">
	            <li><a id="navbar-employees">Snipe Super Sale</a></li>
	            <li><a id="navbar-merchants">Snipe Store</a></li>
	          </ul> -->
	        </li>
          <li><a href="team.php" id="navbar-merchants">Team</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li><a class="contact-link">Contact Us</a></li>
          <!-- <li><a class="snipe-store-link"  href="http://www.snipe.ph/snipe-store"><img src="images/snipe-store-store.png"/>Snipe Super Sale</a></li> -->
          <!-- <li><a class="snipe-store-link"  href="http://www.snipe.ph/snipe-store"><img src="images/snipe-store-bag.png"/>Snipe Store</a></li> -->
        </ul>
      </div>
    </div>
  </nav>
