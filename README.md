# Snipe Solutions Information Website

Built with PHP, jQuery, and Bootstrap CSS.

## Desktop View
![Desktop demo](demo/home-desktop.gif)

## Mobile View
![Mobile demo](demo/home-mobile-small.gif)
