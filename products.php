<?php 

session_start(); 
$_SESSION['page'] = 1;

include 'includes/header.php';

?>

	<div style="height:70px; background-color:white;"></div>

  <!-- Products Section -->
	<div id="product-1" class="product">
		<div class="container">
  		<div class="row">
	  		<div class="product-img col-md-4 product-1 ">
	  			<img src="images/snipe-supersale.png">
	  		</div>
	  		<div class="product-desc col-md-7 desc-1">
	  			<h1 class="visible-md	visible-lg">Snipe Super Sale</h1>
	  			<p>Enjoy a private bazaar of exclusively discounted goods, products and services in the comfort of your own office. Shop with your co-workers and friends for the best deals on electronics, apparel, shoes, accessories, services and many more!</p>
		  		<div class="icon-tray">
		  			<div class="col-md-3 col-xs-6" ><img src="images/discover.png"></div>
		  			<div class="col-md-3 col-xs-6" ><img src="images/fun.png"></div>
		  			<div class="col-md-3 col-xs-6" ><img src="images/money.png"></div>
		  			<div class="col-md-3 col-xs-6" ><img src="images/time.png"></div>
		  		</div>
	  		</div>
  		</div>
		</div>
	</div>

	<div id="product-1-carousel" class="product">
		<div class="container">
  		<div class="row">
			  <div class="supersale-gallery">
	  			<div><img src="images/supersale/bazaar-a.jpg"></div>
	  			<div><img src="images/supersale/bazaar-b.jpg"></div>
	  			<div><img src="images/supersale/bazaar-c.jpg"></div>
	  			<div><img src="images/supersale/bazaar-d.jpg"></div>
	  			<div><img src="images/supersale/bazaar-e.jpg"></div>
	  			<div><img src="images/supersale/bazaar-f.jpg"></div>
			  </div>
			</div>
		</div>
	</div>

	<div id="product-2" class="product">
		<div class="container">
			<div class="row">
	  		<div class="product-img col-md-4 col-md-push-7 product-2">
	  			<img src="images/snipe-store.png">
	  		</div>
	  		<div class="product-desc col-md-7 col-md-pull-4 desc-2">
	  			<h1 class="visible-md	visible-lg">Snipe Store</h1>
					<p>Order exclusively discounted goods, products and services and have them delivered straight to your office using the Snipe Store website. Access discounts from your favorite brands and enjoy convenient online shopping through your laptop and mobile devices.</p>
	  			<div class="icon-tray row">
		  			<div class="col-md-3 col-xs-6" ><img src="images/discover.png"></div>
		  			<div class="col-md-3 col-xs-6" ><img src="images/fun.png"></div>
		  			<div class="col-md-3 col-xs-6" ><img src="images/money.png"></div>
		  			<div class="col-md-3 col-xs-6" ><img src="images/time.png"></div>
		  		</div>
	  		</div>
			</div>
		</div>
	</div>
			
	<div id="product-2-steps" class="product">
		<div class="container">
			<div class="row">
	  		<div class="snipestore-screen-view col-md-5">
	  			<img src="images/snipestore/snipestore-laptop-events.png">
	  		</div>
	  		<div class="product-desc col-md-7 desc-1">
	  			<h1>It's Online!</h1>
	  			<p>Register online through a unique SNIPE CODE once your company is a registered corporate partner!</p>
	  		</div>
			</div>
			
			<div class="row">
	  		<div class="snipestore-screen-view col-md-5 col-md-push-7">
	  			<img src="images/snipestore/snipestore-tablet-phone-products.png">
	  		</div>
	  		<div class="product-desc col-md-7 col-md-pull-5 desc-1">
	  			<h1>With a Wide Variety!</h1>
	  			<p>Choose from a wide-array of products with just one click and have your products delivered straight to your office!</p>
	  		</div>
			</div>
		</div>
	</div>

  <?php include 'includes/footer.php';?>