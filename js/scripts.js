
$(document).ready(function(){      

	// scroll to main sections
	$('#employee_header, #navbar-employees').click(function() {
	    $('html, body').animate({
	        scrollTop: $('#employee').offset().top - 70
	    }, 1000);
	});
	$('#merchant_header, #navbar-merchants').click(function() {
	    $('html, body').animate({
	        scrollTop: $('#merchant').offset().top - 70
	    }, 1000);
	});
	$('#corp_header, #navbar-corporations').click(function() {
	    $('html, body').animate({
	        scrollTop: $('#corporate').offset().top - 70
	    }, 1000);
	});
	$('.contact-link').click(function() {
	    $('html, body').animate({
	        scrollTop: $('#contact_us').offset().top
	    }, 1000);
	});

	// scroll onclick: landing view
	$('#benefits-button, #logo-button').click(function() {
			scrollViaLogo = true;
	    $('html, body').animate({
	        scrollTop: $('#snipe_jumbotron').offset().top
	    }, 1000);
	});	

	// caret's scroll
	$('.caret-down').click(function() {
	    $('html, body').animate({
	        scrollTop: $('#product-preview').offset().top - 70
	    }, 1000);
	});

	// Contact Us
	$('.contact-icon').click(function() {
		$('.contact-input').prop('disabled', false);
	});

	$('.icon-employee').click(function() {
    $('#position-field').slideDown('slow');
		$('#form-position').prop('placeholder', 'Position');
	});
	$('.icon-merchant').click(function() {
    $('#position-field').slideDown('slow');
		$('#form-position').attr('placeholder', 'Brand');
	});
	$('.icon-corporation').click(function() {
    $('#position-field').slideDown('slow');
		$('#form-position').prop('placeholder', 'Company');
	});

	$( '#modal-ok-button' ).click(function (){
		$( '.form-group' ).css("display", "none");
		$( '.contact-success' ).css("display", "block");
	});

});

// Parallax 
function parallax() {
	// Turn parallax scrolling off for iOS devices
  var iOS = false,
    p = navigator.platform;
  if (p === 'iPad' || p === 'iPhone' || p === 'iPod') {
    iOS = true;
  }

	var scrollTop = $(window).scrollTop();
	var scaleBg = -$(window).scrollTop() / 3;

	if (iOS === false) {
		if (scrollTop > 689 && scrollTop < 1895) {
			$('.parallax-1').css('background-position-y', scaleBg - 280);
		};
		if (scrollTop > 1700 && scrollTop < 2919) {
			$('.parallax-2').css('background-position-y', scaleBg - 280);
		};
	}
}

$(document).ready(function () {
	var browserWidth = $(window).width();
	if (browserWidth > 560){ 
		$(window).scroll(function() {
			parallax();
		});
	}
});

// products preview carousel
$('#employee_carousel').carousel({
  interval: 5000
})

// corporate partners carousel
$(document).ready(function(){
  $('.corporate-carousel').slick({
	  infinite: true,
	  slidesToShow: 3,
	  slidesToScroll: 1,
	  autoplay: true,
	  autoplaySpeed: 1000,     
	  arrows: false,
	  responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 3,
	        infinite: true,
	        dots: true
	      }
	    },
	    {
	      breakpoint: 600,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }]
  });
});
//
$('.supersale-gallery').slick({
  dots: false,
  infinite: true,
  speed: 300,
  slidesToShow: 1,
  centerMode: true,
  variableWidth: true,
  autoplay: true,
  autoplaySpeed: 2000,
  arrows: false
});	

$("#contact-form").submit(function(e){
  e.preventDefault();
  $.ajax({
    type : 'POST',
    data: $("#contact-form").serialize(),
    url : 'includes/mailer.php',
    success : function(data){
    	if (data == 'true') {
    		console.log(data);
				$( '.form-group' ).css("display", "none");
				$( '.contact-success' ).css("display", "block");
			} else {
				$( '.contact-failure' ).css("display", "block");
			};
    }
  });
  return false;
});
