<?php 

session_start(); 
$_SESSION['page'] = 1;

include 'includes/header.php';

?>

	<div id="team-details">
		<div class="container">

	  	<div class="row">

	  		<div class="col-md-4">
	  			<div class="team-portrait">
	  				<img src="images/team/1.jpg">
	  				<div class="overlay-cover"></div>
	  				<div class="overlay-icons">
	  					<div>
	  						<div class="row">
	  						<!-- <a class="col-md-6" href=""><img src="images/team/black-facebook.png"></a> -->
	  						<a class="col-md-6" href="https://ph.linkedin.com/in/robert-suyom-8b738b39" target="_blank"><img src="images/team/black-linkedin.png"></a>
	  						</div>
	  						<div class="row">
	  						<!-- <a class="col-md-6" href=""><img src="images/team/black-twitter.png"></a> -->
	  						<!-- <a class="col-md-6" href=""><img src="images/team/black-googleplus.png"></a> -->
	  						</div>
	  					</div>
	  				</div>
	  			</div>
	  			<div>
		  			<h4>Robert Suyom</h4>
		  			<h5>CEO</h5>
	  			</div>
	  		</div>

	  		<div class="col-md-4">
	  			<div class="team-portrait">
	  				<img src="images/team/2.jpg">
	  				<div class="overlay-cover"></div>
	  				<div class="overlay-icons">
	  					<div>
	  						<div class="row">
	  						<!-- <a class="col-md-6" href=""><img src="images/team/black-facebook.png"></a> -->
	  						<a class="col-md-6" href="https://ph.linkedin.com/in/pocholo-sebastian-a2625437" target="_blank"><img src="images/team/black-linkedin.png"></a>
	  						</div>
	  						<div class="row">
	  						<!-- <a class="col-md-6" href=""><img src="images/team/black-twitter.png"></a> -->
	  						<!-- <a class="col-md-6" href=""><img src="images/team/black-googleplus.png"></a> -->
	  						</div>
	  					</div>
	  				</div>
	  			</div>
	  			<div>
		  			<h4>Cholo Sebastian</h4>
		  			<h5>CFO</h5>
	  			</div>
	  		</div>

	  		<div class="col-md-4">
	  			<div class="team-portrait">
	  				<img src="images/team/3.jpg">
	  				<div class="overlay-cover"></div>
	  				<div class="overlay-icons">
	  					<div>
	  						<div class="row">
	  						<!-- <a class="col-md-6" href=""><img src="images/team/black-facebook.png"></a> -->
	  						<a class="col-md-6" href="https://ph.linkedin.com/in/marc-carlo-lijauco-a1994a5b" target="_blank"><img src="images/team/black-linkedin.png"></a>
	  						</div>
	  						<div class="row">
	  						<!-- <a class="col-md-6" href=""><img src="images/team/black-twitter.png"></a> -->
	  						<!-- <a class="col-md-6" href=""><img src="images/team/black-googleplus.png"></a> -->
	  						</div>
	  					</div>
	  				</div>
	  			</div>
	  			<div>
		  			<h4>Marc Carlo Lijauco</h4>
		  			<h5>CTO</h5>
	  			</div>
	  		</div>
  		</div>

	  	<div class="row">
	  		<div class="col-md-4 col-md-offset-2">
	  			<div class="team-portrait">
	  				<img src="images/team/4.jpg">
	  				<div class="overlay-cover"></div>
	  				<div class="overlay-icons">
	  					<div>
	  						<div class="row">
	  						<!-- <a class="col-md-6" href=""><img src="images/team/black-facebook.png"></a> -->
	  						<a class="col-md-6" href="https://www.linkedin.com/in/ricianne-marie-rey-80532739" target="_blank"><img src="images/team/black-linkedin.png"></a>
	  						</div>
	  						<div class="row">
	  						<!-- <a class="col-md-6" href=""><img src="images/team/black-twitter.png"></a> -->
	  						<!-- <a class="col-md-6" href=""><img src="images/team/black-googleplus.png"></a> -->
	  						</div>
	  					</div>
	  				</div>
	  			</div>
	  			<div>
		  			<h4>Chi Rey</h4>
		  			<h5>Marketing Director</h5>
	  			</div>
	  		</div>

	  		<div class="col-md-4">
	  			<div class="team-portrait">
	  				<img src="images/team/5.jpg">
	  				<div class="overlay-cover"></div>
	  				<div class="overlay-icons">
	  					<div>
	  						<div class="row">
	  						<!-- <a class="col-md-6" href=""><img src="images/team/black-facebook.png"></a> -->
	  						<a class="col-md-6" href="https://www.linkedin.com/in/justin-tungul-87962287" target="_blank"><img src="images/team/black-linkedin.png"></a>
	  						</div>
	  						<div class="row">
	  						<!-- <a class="col-md-6" href=""><img src="images/team/black-twitter.png"></a> -->
	  						<!-- <a class="col-md-6" href=""><img src="images/team/black-googleplus.png"></a> -->
	  						</div>
	  					</div>
	  				</div>
	  			</div>
	  			<div>
		  			<h4>Justin Tungul</h4>
		  			<h5>Solutions Director</h5>
	  			</div>
	  		</div>
	  	</div>

	  </div>
	</div>

	<div id="awards" class="container-fluid" style="background-image: url(images/team/awards-bg.jpg);">
		<div class="overlay"></div>
	  <!-- <img class="award-logo" src="images/team/award-gray.png"> -->
	  <h1 class="subtitle">Awards</h1>
	  <div class="borderbottom"></div>
		<div class="company-logos row">
		  <div class="col-md-4"><img src="images/team/award-2.png"></div>
		  <div class="col-md-4"><img src="images/team/award-3.png"></div>
		  <div class="col-md-4"><img src="images/team/award-1.png"></div>
	  </div>
	  <h1 class="subtitle">Affiliations</h1>
	  <div class="borderbottom"></div>
		<div class="company-logos row">
		  <div class="col-md-4 col-md-offset-2"><img src="images/team/affiliate-3.png"></div>
		  <div class="col-md-4"><img src="images/team/affiliate-2.png"></div>
	  </div>
	</div>
	
  <?php include 'includes/footer.php';?>