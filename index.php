<?php 

session_start(); 
$_SESSION['page'] = 0;

include 'includes/header.php';

?>

  <!-- Snipe Landing View -->
	<div id="snipe_jumbotron" style="background-image: url('images/snipe-bg-v3.jpg')">
			<img class="snipe-logo" src="images/logo.png">
			<p class="subtitle">Your one-stop solutions partner for your employee <br/>engagement and business needs</p>
			<p class="caret-down glyphicon glyphicon-chevron-down"></p>
	</div>


  <!-- Snipe Landing View -->
	<div id="product-preview">
		<div class="container">
			<div id="employee_carousel" class="carousel slide" data-ride="carousel">
			  <!-- Indicators -->
			  <ol class="carousel-indicators">
			    <li data-target="#employee_carousel" data-slide-to="0" class="active"></li>
			    <li data-target="#employee_carousel" data-slide-to="1"></li>
			  </ol>

			  <!-- Wrapper for slides -->
			  <div class="carousel-inner" role="listbox">
			    <div class="item active">
			    	<div class="super-sale-image col-md-5">
			    		<img src="images/snipe-supersale.png"/>
			    	</div>
				  	<div class="super-sale-content col-md-7">
	  					<h1>The Snipe Super Sale</h1>
	  					<p>Enjoy a private bazaar of exclusively discounted goods, products and services in the comfort of your own office. Shop with your co-workers and friends for the best deals on electronics, apparel, shoes, accessories, services and many more!</p>
	  					<a href="">Learn More</a>
				  	</div>
			    </div>
			    <div class="item">		
			    	<div class="store-image col-md-5">
			    		<img src="images/snipe-store.png"/>
			    	</div>
				  	<div class="store-content col-md-7">
	  					<h1>The Snipe Store</h1>
  						<p>Order exclusively discounted goods, products and services and have them delivered straight to your office using the Snipe Store website. Access discounts from your favorite brands and enjoy convenient online shopping through your laptop and mobile devices.</p>
	  					<a href="">Learn More</a>
				  	</div>
			    </div>

			  </div>
			</div>
		</div>
	</div>


  <!-- Employee Section -->
	<div id="employee" name="employee">
		<div class="container">
	  	<div class="section-header">
	  		<p><span>Snipe for employees</span></p>
	  	</div>
	  	<div class="section-content">
	  		<div class="row">
			  	<div class="col-md-4">
			  		<div><img src="images/section-icons/1-exclusive-discounts.png"></div>
			  		<h2>Access to Exclusive<br/>Discounts</h2>
			  		<p>Get deals specially tailored for you and your co-workers. Enjoy better discounts than malls and other online stores from your favorite brands!</p>
			  	</div>
			  	<div class="col-md-4">
			  		<div><img src="images/section-icons/2-exclusive-rewards-and-benefits.png"></div>
			  		<h2>Personalized Benefits and<br/>Activities</h2>
			  		<p>Enjoy discounts from the brands that you like most and engage in activities that fit your interest best!</p>
			  	</div>
			  	<div class="col-md-4">
			  		<div><img src="images/section-icons/3-hassle-free-shopping.png"></div>
			  		<h2>Convenient and Hassle-Free<br/>Shopping</h2>
			  		<p>Shop at your own convenience within the office premises through our on-site and online selling events!</p>
			  	</div>
		  	</div>
	  	</div>
		</div>
	</div>


  <!-- <section class="parallax parallax-1 parallax-office"></section> -->


	<!-- Merchant Section -->
	<div id="merchant">
	  <div class="container">
	  	<div class="section-header">
	  		<p><span>Snipe for merchants</span></p>
	  	</div>
	  	<div class="section-content">
	  		<div class="row">
			  	<div class="col-md-4">
			  		<div><img src="images/section-icons/4-merchant.png"></div>
			  		<h2>Exclusive Corporate Sales and<br/>Marketing Arm</h2>
			  		<p>Expand sales and marketing opportunities by tapping Snipe&#39;s intensive corporate-partner network.</p>
			  	</div>
			  	<div class="col-md-4">
			  		<div><img src="images/section-icons/5-access-to-technology.png"></div>
			  		<h2>Access to Technology and<br/>Analytics</h2>
			  		<p>Use the best platforms to sell products and use analytics to make informed decisions and strategies for your business.</p>
			  	</div>
			  	<div class="col-md-4">
			  		<div><img src="images/section-icons/6-expert-solutions-3.png"></div>
			  		<h2>Expert Support and<br/>Assistance</h2>
			  		<p>Increase brand engagement and sales performance through the help of our team of IT and marketing experts.</p>
			  	</div>
		  	</div>
	  	</div>
	  </div>
	</div>


  <!-- <section class="parallax parallax-2 parallax-employee"></section> -->


  <!-- Corporate Section -->
	<div id="corporate">
  	<div class="container">
	  	<div class="section-header">
	  		<p><span>Snipe for companies</span></p>
	  	</div>
  		
	  	<div class="section-content">
	  		<div class="row">
			  	<div class="col-md-4">
			  		<div><img src="images/section-icons/7-employee.png"></div>
			  		<h2>Tailored Employee<br/>Benefits</h2>
			  		<p>Work with our team in planning and crafting additional benefits that fit your employees&#39; interests and preferences best.</p>
			  	</div>
			  	<div class="col-md-4">
			  		<div><img src="images/section-icons/8-corporate-partner.png"></div>
			  		<h2>Dynamic Engagement<br/>Activities</h2>
			  		<p>Create flexible engagement activities that fit your company&#39;s thrusts. Use Snipe to promote work-life balance, employee wellness, role diversity and many more.</p>
			  	</div>
			  	<div class="col-md-4">
			  		<div><img src="images/section-icons/9-expert-support-and-assistance.png"></div>
			  		<h2>Free and Easy-To-Use<br/>Platforms</h2>
			  		<p>Enjoy complete Snipe services and support for FREE!</p>
			  	</div>
		  	</div>
	  	</div>

	  	<h1 class="corporate-carousel-header">Our Partners</h1>
		  <div class="corporate-carousel">
  			<div><img src="images/corporate-logos/hpe.png"></div>  			
  			<div><img src="images/corporate-logos/sykes.png"></div>
  			<div><img src="images/corporate-logos/tata.png"></div>
  			<div><img src="images/corporate-logos/amex-1.png"></div>
  			<div><img src="images/corporate-logos/anytimefitness.png"></div>
		  </div>
	  </div>
  </div>

  <?php include 'includes/footer.php';?>
